include("MC15JobOptions/Sherpa_2.2.5_NNPDF30NNLO_Common.py")
evgenConfig.description = "Sherpa ttZ+1j@NLO"
evgenConfig.keywords = ["ttZ", "SM"]
evgenConfig.contact = ["frank.siegert@cern.ch","narayan@cern.ch"]
evgenConfig.inputconfcheck = "ttZnnqq"
genSeq.Sherpa_i.RunCard="""
(run){
    HARD_DECAYS=1;
    STABLE[6] = 0; WIDTH[6]=0.0;
    STABLE[23] = 0; WIDTH[23]=0.0;
    STABLE[24] = 0;
    ACTIVE[25] = 0;
    
    CORE_SCALE VAR{H_TM2/4}
    EXCLUSIVE_CLUSTER_MODE 1;
    
    # merging setup
    QCUT:=30.;
    LJET:=3,4; NJET:=2;
    
    ME_SIGNAL_GENERATOR Comix Amegic OpenLoops;
    INTEGRATION_ERROR=0.05;
    
    #Disable leptonic decays
    HDH_STATUS[23,11,-11]=0
    HDH_STATUS[23,13,-13]=0
    HDH_STATUS[23,15,-15]=0
}(run)

(processes){
    Process 93 93 -> 6 -6 23 93{NJET};
    Order (*,1);
    CKKW sqr(QCUT/E_CMS);
    NLO_QCD_Mode MC@NLO {LJET}; 
    ME_Generator Amegic {LJET};
    RS_ME_Generator Comix {LJET};
    Loop_Generator OpenLoops;
    End process;
}(processes)
"""
genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0.0","WIDTH[23]=0.0" ]
genSeq.Sherpa_i.NCores = 64
genSeq.Sherpa_i.OpenLoopsLibs = [ "ppztt", "ppzttj"]
